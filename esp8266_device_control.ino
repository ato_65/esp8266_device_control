/*
 *Questo progetto permette di abilitare da locale o da 
 *remoto qualsiasi cosa controllabile tramite relay, 
 *interfaccia comando remoto www.Blynk.io
 */
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

// inserisci Auth Token di Blynk App.
char auth[] = "tuo_token";

// credenziali per il WiFi.
char ssid[] = "tua_rete_wifi";
char pass[] = "tua_password";

#include "Pulsante.h"
Pulsante pl1 (13, 14); //(D7, D5) abilitazione relè
Pulsante pl2 (13, 4);  //(D7, D2) feedback led locale

const int L2 = 0; //led blu indica connessione stabilita (pin D3)
const int S1 = 12; //ingresso digitale esterno per running (pin D6)

// quando si connette al server
BLYNK_CONNECTED() {
// sincronizza l'ultimo stato del server
  Blynk.syncVirtual(V2);
}

// quando si preme il pulsante virtuale, cambiare stato
BLYNK_WRITE(V2) {
  pl1.outState = param.asInt();
  pl2.outState = param.asInt();
}

BlynkTimer timer;

void myTimerEvent() {
  Blynk.virtualWrite(V2, pl1.outState);
  Blynk.virtualWrite(V2, pl2.outState);
}

void setup() {
  Serial.begin(9600);
  Blynk.begin(auth, ssid, pass, "tuo_server", 8080);
  timer.setInterval(500L, myTimerEvent);
  pinMode(L2, OUTPUT);
  pinMode(S1, INPUT);
}

void loop() {
  pl1.premiPuls();
  pl2.premiPuls();                 
  Blynk.run(); // Blynk in esecuzione
  timer.run();

    //abilitazione del led blu per connessione al server
  if (Blynk.connected())  {
    digitalWrite(L2, HIGH);
  }
  else  {
    digitalWrite(L2, LOW);
  }
}
