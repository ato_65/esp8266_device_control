# INTERRUTTORE SMART-- tEST MAURO

Questo progetto nasce in un contesto di studio del [Gruppo Utenti Linux Livorno](https://linux.livorno.it/sito/), un modo per esplorare le varie tecniche di condivisione di un progetto , oltre chiaramente allo sviluppo concreto di un dispositivo, integrato in un sistema  open source autonomo.

## Descrizione 
Questo progetto permette di abilitare da locale o da remoto qualsiasi cosa controllabile tramite relay, ad esempio: 

*  luci, 
*  serranda, 
*  cancello, 
*  start/stop di un macchinario, 
*  ecc. 
  
Si potrà quindi modificare lo stato dell'attiviazione da accesso a spento o viceversa, sia tramite un pulsante locale, che tramite un pulsante virtuale sull'applicazione installata sul cellulare.

## Hardware di riferimento
* [wemos D1 mini](https://www.wemos.cc/en/latest/d1/index.html)
* pulsante
* led stato connessione al server
* led stato del relay
* alimentazione...


## Piattaforma software

Public and Private Server [Blynk-server](https://github.com/blynkkk/blynk-serverhttps://blynk.io/)

Ambiente di sviluppo [Arduino IDE](https://www.arduino.cc/en/main/software#)

Mobile-App [Blynk for Android](https://play.google.com/store/apps/details?id=cc.blynk&hl=it) [Blynk for iPhone](https://apps.apple.com/us/app/blynk-iot-for-arduino-esp32/id808760481)


## Altro
..
